package sprint.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

@Entity
@XmlRootElement
@XmlSeeAlso({Developer.class})
public class Team {

    protected Long id;
    protected String name;
    protected List<Developer> developer = new ArrayList<Developer>();

    public Team() {
        super();
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @XmlElementRef
    @OneToMany(mappedBy = "team", cascade = CascadeType.PERSIST)
    public List<Developer> getDeveloper() {
        return developer;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeveloper(List<Developer> developer) {
       this.developer = developer;
    }

    @Override
    public String toString() {
        return "Team [id=" + id + ", name=" + name + "]";
    }
}
