package sprint.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import sprint.domain.util.DeveloperSerializer;
import sprint.domain.util.UserStorySerializer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * @author Yvanne CHAUSSIN & Olivier GUILLOU
 *         <h1>TP1 Pattern MVC - TAA architecture</h1> 8 oct. 2015
 */
@Entity
@XmlRootElement
@XmlSeeAlso({Developer.class, UserStory.class})
public class Task {

	/**
	 * The Id.
	 */
	private Long id;
	/**
	 * The name of the task.
	 */
	private String name;
    /**
     *
     */
	private Priority priority;
	/**
	 * Description of the task
	 */
	private String description;
    /**
     *
     */
    private Status status;
    /**
     *
     */
	private Developer developer;
    /**
     *
     */
	private UserStory userStory;

	/**
	 * default constructor.
	 */
	public Task() {
		super();
	}

	/**
	 * @return the id
	 */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

    /**
     * @return the priority
     */
    @Enumerated(EnumType.ORDINAL)
    public Priority getPriority() {
        return priority;
    }

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


    @Enumerated(EnumType.ORDINAL)
    public Status getStatus() {
        return status;
    }

    /**
     * @return the user
     */
    @XmlElementRef
    @ManyToOne
    @JsonSerialize(using = DeveloperSerializer.class)
    public Developer getDeveloper() {
        return developer;
    }

    /**
     * @return the userStory
     */
    @XmlElementRef
    @ManyToOne
    @JsonSerialize(using = UserStorySerializer.class)
    public UserStory getUserStory() {
        return userStory;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

    public void setStatus(Status status) {
        this.status = status;
    }

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

    /**
     * @param developer
     *            the developer to set
     */
    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    /**
     * @param userStory
     */
    public void setUserStory(UserStory userStory) {
     this.userStory = userStory;
     }

    @Override
    public String toString() {
        return " [id=" + id + ", name=" + name + "]";
    }

}
