package sprint.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import sprint.domain.util.EpicSerializer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yvanne CHAUSSIN & Olivier GUILLOU
 * <h1>TP1 Pattern MVC - TAA architecture </h1>
 *         20 oct. 2015
 */
@Entity
@XmlRootElement
@XmlSeeAlso({Epic.class})
public class UserStory {

	/**
	 * Id of the user story
	 */
	private Long id;
	/**
	 * Name of the user story
	 */
	private String name;
	/**
	 * Description of the user story
	 */
	private String description;
	/**
	 * Priority of the user story
	 */
	private Priority priority;
	/**
	 * Tasks related to the user story
	 */
	private List<Task> tasks = new ArrayList<Task>();

	private Epic epic;

	/**
	 * Default constructor.
	 */
	public UserStory() {
		super();
	}

	/**
	 * Return the id of the user story
	 * @return Id of the user story
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	/**
	 * Return the name of the user story
	 * @return Name of the user story
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Return the description of the user story
	 * @return Description of the user story
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Return the priority of the user story
	 * @return Priority of the user story
	 */
	@Enumerated(EnumType.ORDINAL)
	public Priority getPriority() {
		return this.priority;
	}

	/**
	 * Return all the tasks related to the user story
	 * @return Tasks related to the user story
	 */
	@OneToMany(mappedBy = "userStory", cascade = CascadeType.PERSIST)
	public List<Task> getTasks() {
		return this.tasks;
	}

	/**
	 * Return the epic from which the userstory is a part of.
	 * @return The epic
	 */
	@ManyToOne
	@JsonSerialize(using = EpicSerializer.class)
	public Epic getEpic() {
		return this.epic;
	}

	/**
	 * Set the id of the user story
	 * @param id Id of the user story
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Modify the name of the user story
	 * @param name Name of the user story
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Modify description of the user story
	 * @param description Description of the user story
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set the priority of the user story
	 * @param priority Priority of the user story
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	/**
	 * Modify the list of tasks
	 * @param tasks List of tasks
	 */
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	/**
	 * Modify the epic from which the userstory is a part of.
	 * @param epic The Epic
	 */
	public void setEpic(Epic epic) {
		this.epic = epic;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserStory [id=" + id + ", name=" + name + ", description=" + description + ", priority=" + priority
				+ ", tasks=" + tasks + ", epic=" + epic + "]";
	}
}
