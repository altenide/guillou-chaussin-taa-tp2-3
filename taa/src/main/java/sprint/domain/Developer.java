package sprint.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import sprint.domain.util.TeamSerializer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.HashSet;
import java.util.Set;

@Entity
@XmlRootElement
@XmlSeeAlso({Team.class})
public class Developer {

	/**
	 *
	 */
	protected Long id;
	/**
	 * The name of the user.
	 */
	protected String name;
	/**
	 * Last Name of the use
	 */
	protected String surname;
	/**
	 *
	 */
	protected Set<Task> tasks = new HashSet<Task>();

	protected Team team;

	public Developer() {
		super();
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/**
	 * Return the name of the developer
	 *
	 * @return Name of the developer
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @return the tasks
	 */
	@OneToMany(mappedBy = "developer", cascade = CascadeType.PERSIST)
	public Set<Task> getTasks() {
		return tasks;
	}

	@ManyToOne
	@JsonSerialize(using = TeamSerializer.class)
	public Team getTeam() {
		return team;
	}

	/**
	 * @param id the idForUser to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Modify the developer's name
	 *
	 * @param name
	 *            Name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param surname
	 *            the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @param tasks
	 *            the tasks to set
	 */
	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Developer [id=" + id + ", name=" + name + ", surname=" + surname
				+ ", tasks=" + tasks + ", team=" + team + "]";
	}
}
