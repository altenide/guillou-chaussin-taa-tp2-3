package sprint.domain;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum Status {
    NOT_STARTED, ONGOING, DONE
}
