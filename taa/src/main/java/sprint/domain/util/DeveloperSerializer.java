package sprint.domain.util;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import sprint.domain.Developer;

import java.io.IOException;

public class DeveloperSerializer extends JsonSerializer<Developer> {

    @Override
    public void serialize(Developer dev, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException {
        generator.writeNumber(dev.getId());
    }
}
