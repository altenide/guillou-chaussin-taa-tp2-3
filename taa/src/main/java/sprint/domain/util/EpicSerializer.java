package sprint.domain.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import sprint.domain.Epic;

import java.io.IOException;

public class EpicSerializer extends JsonSerializer<Epic> {

    @Override
    public void serialize(Epic epic, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException {
        generator.writeNumber(epic.getId());
    }
}
