package sprint.domain.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import sprint.domain.UserStory;

import java.io.IOException;

public class UserStorySerializer extends JsonSerializer<UserStory> {

    @Override
    public void serialize(UserStory userStory, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException {
        generator.writeNumber(userStory.getId());
    }
}
