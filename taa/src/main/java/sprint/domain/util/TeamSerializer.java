package sprint.domain.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import sprint.domain.Team;

import java.io.IOException;

public class TeamSerializer extends JsonSerializer<Team> {

    @Override
    public void serialize(Team team, JsonGenerator generator, SerializerProvider serializerProvider)
            throws IOException {
        generator.writeNumber(team.getId());
    }
}
