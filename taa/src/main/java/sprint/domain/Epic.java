package sprint.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

@Entity
@XmlRootElement
@XmlSeeAlso({UserStory.class})
public class Epic {

    private Long id;
    private String name;
    private Priority priority;
    private List<UserStory> userstory = new ArrayList<UserStory>();

    /**
     * Default constructor.
     */
    public Epic() {
        super();
    }

    /**
    * @return the id
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
    * @return the name
    */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    @Enumerated(EnumType.ORDINAL)
    public Priority getPriority() {
        return priority;
    }

    /**
     *
     * @return
     */
    @XmlElementRef
    @OneToMany(mappedBy = "epic", cascade = CascadeType.PERSIST)
    public List<UserStory> getUserstory() {
        return this.userstory;
    }

    /**
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param priority
     */
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    /**
     *
     * @param userstory
     */
    public void setUserstory(List<UserStory> userstory) {
        this.userstory = userstory;
    }
}
