package sprint.domain;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum Priority {
	low,
	medium,
	high
}