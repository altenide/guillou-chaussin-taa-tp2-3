package sprint.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import sprint.domain.UserStory;

public interface UserStoryDAO extends JpaRepository<UserStory,Long> {

}
