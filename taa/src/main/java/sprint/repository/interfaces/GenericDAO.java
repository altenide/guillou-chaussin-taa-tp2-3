package sprint.repository.interfaces;

import javax.persistence.EntityManager;
import java.util.List;

public interface GenericDAO<T, PK> {

    String PERSISTENCE_UNIT_SQL = "mysqlpersistence";

    T create(T t);
    T findOne(PK id);
    List<T> findAll();
    void delete(T t);
    T update(T t);

    void setManager(EntityManager manager);
}
