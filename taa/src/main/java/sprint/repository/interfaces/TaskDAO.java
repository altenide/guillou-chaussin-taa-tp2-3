package sprint.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import sprint.domain.Task;

public interface TaskDAO extends JpaRepository<Task, Long> {

}
