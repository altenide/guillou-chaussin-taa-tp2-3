package sprint.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import sprint.domain.Epic;

public interface EpicDAO extends JpaRepository<Epic, Long> {

}
