package sprint.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import sprint.domain.Team;

public interface TeamDAO extends JpaRepository<Team, Long> {

}
