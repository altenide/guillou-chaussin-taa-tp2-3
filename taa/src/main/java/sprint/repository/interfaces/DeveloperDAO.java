package sprint.repository.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sprint.domain.Developer;

@Repository
public interface DeveloperDAO extends JpaRepository<Developer,Long> {

}
