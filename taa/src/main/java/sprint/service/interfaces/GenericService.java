package sprint.service.interfaces;

import java.util.List;

public interface GenericService<T, PK> {
    T get(PK id);
    List<T> getAll();
    T create(T t);
    void delete(PK id);
    T update(T t);
}
