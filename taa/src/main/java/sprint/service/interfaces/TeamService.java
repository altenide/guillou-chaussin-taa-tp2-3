package sprint.service.interfaces;

import sprint.domain.Team;

public interface TeamService extends GenericService<Team,Long> {

}
