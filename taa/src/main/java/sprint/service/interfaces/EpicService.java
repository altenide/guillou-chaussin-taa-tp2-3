package sprint.service.interfaces;

import sprint.domain.Epic;

public interface EpicService extends GenericService<Epic,Long> {
}
