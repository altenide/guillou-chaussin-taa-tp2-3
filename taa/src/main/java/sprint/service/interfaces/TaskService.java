package sprint.service.interfaces;

import sprint.domain.Task;

public interface TaskService extends GenericService<Task,Long> {
}
