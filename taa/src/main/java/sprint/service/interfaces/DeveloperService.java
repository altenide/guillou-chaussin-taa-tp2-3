package sprint.service.interfaces;

import sprint.domain.Developer;

public interface DeveloperService extends GenericService<Developer, Long> {
}
