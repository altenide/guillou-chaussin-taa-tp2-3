package sprint.service.interfaces;

import sprint.domain.UserStory;

public interface UserStoryService extends GenericService<UserStory,Long> {
}
