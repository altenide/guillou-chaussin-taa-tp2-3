package sprint.service.impl;

import sprint.domain.UserStory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sprint.repository.interfaces.UserStoryDAO;
import sprint.service.interfaces.UserStoryService;

import java.util.List;

@Service(value = "userStoryService")
@Transactional
public class UserStoryServiceImpl implements UserStoryService {

    @Autowired
    protected UserStoryDAO userStoryDAO;

    @Override
    public UserStory get(Long id) {
        return userStoryDAO.findOne(id);
    }

    @Override
    public List<UserStory> getAll() {
        return userStoryDAO.findAll();
    }

    @Override
    public UserStory create(UserStory userStory) {
        return userStoryDAO.save(userStory);
    }

    @Override
    public void delete(Long id) {
        userStoryDAO.delete(id);
    }

    @Override
    public UserStory update(UserStory userStory) {
        return userStoryDAO.save(userStory);
    }
}
