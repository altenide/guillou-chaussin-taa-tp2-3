package sprint.service.impl;

import sprint.domain.Epic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sprint.repository.interfaces.EpicDAO;
import sprint.service.interfaces.EpicService;

import java.util.List;

@Service
public class EpicServiceImpl implements EpicService {

    @Autowired
    protected EpicDAO epicDAO;

    @Override
    public Epic get(Long id) {
        return epicDAO.findOne(id);
    }

    @Override
    public List<Epic> getAll() {
        return epicDAO.findAll();
    }

    @Override
    public Epic create(Epic epic) {
        epic.setId(null);
        return epicDAO.save(epic);
    }

    @Override
    public void delete(Long id) {
        Epic epic = epicDAO.findOne(id);
        if(epic != null)
            epicDAO.delete(epic);
    }

    @Override
    public Epic update(Epic epic) {
        return epicDAO.save(epic);
    }
}
