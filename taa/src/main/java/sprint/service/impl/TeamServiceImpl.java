package sprint.service.impl;

import sprint.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sprint.repository.interfaces.TeamDAO;
import sprint.service.interfaces.TeamService;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    protected TeamDAO teamDAO;

    @Override
    public Team get(Long id) {
        return teamDAO.findOne(id);
    }

    @Override
    public List<Team> getAll() {
        return teamDAO.findAll();
    }

    @Override
    public Team create(Team team) {
        return teamDAO.save(team);
    }

    @Override
    public void delete(Long id) {
        Team team = teamDAO.findOne(id);
        if (team != null)
            teamDAO.delete(team);
    }

    @Override
    public Team update(Team team) {
        return teamDAO.save(team);
    }
}
