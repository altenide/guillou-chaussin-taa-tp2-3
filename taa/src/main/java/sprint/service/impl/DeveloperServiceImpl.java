package sprint.service.impl;

import sprint.domain.Developer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import sprint.repository.interfaces.DeveloperDAO;
import sprint.service.interfaces.DeveloperService;

import java.util.List;

@Service
@Transactional
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    protected DeveloperDAO developerDAO;

    @Override
    public Developer get(Long id) {
        return developerDAO.findOne(id);
    }

    @Override
    public List<Developer> getAll() {
        return developerDAO.findAll();
    }

    @Override
    public Developer create(Developer developer) {
        developer.setId(null);
        return developerDAO.save(developer);
    }

    @Override
    public void delete(Long id) {
        Developer developer = developerDAO.findOne(id);
        if (developer != null)
            developerDAO.delete(developer);
    }

    @Override
    public Developer update(Developer developer) {
        return developerDAO.save(developer);
    }
}
