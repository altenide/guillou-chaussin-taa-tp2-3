package sprint.service.impl;

import sprint.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sprint.repository.interfaces.TaskDAO;
import sprint.service.interfaces.TaskService;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    protected TaskDAO taskDAO;

    @Override
    public Task get(Long id) {
        return taskDAO.findOne(id);
    }

    @Override
    public List<Task> getAll() {
        return taskDAO.findAll();
    }

    @Override
    public Task create(Task task) {
        task.setId(null);
        return taskDAO.save(task);
    }

    @Override
    public void delete(Long id) {
        Task task = taskDAO.findOne(id);
        if (task != null)
            taskDAO.delete(task);
    }

    @Override
    public Task update(Task task) {
        return taskDAO.save(task);
    }
}
