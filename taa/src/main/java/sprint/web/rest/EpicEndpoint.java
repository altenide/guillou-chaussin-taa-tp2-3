package sprint.web.rest;

import org.springframework.web.bind.annotation.*;
import sprint.domain.Epic;

import org.springframework.beans.factory.annotation.Autowired;

import sprint.service.interfaces.EpicService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/epic")
public class EpicEndpoint {

    @Autowired
    private EpicService epicService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Epic> getEpics() {
        return epicService.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Epic getEpic(@PathVariable("id") long id) {
        return epicService.get(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Epic addEpic(@RequestBody Epic epic)  {
        return epicService.create(epic);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Epic updateEpic(@RequestBody Epic epic)  {
        return epicService.update(epic);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void removeEpic(@PathVariable("id") Long id) {
        epicService.delete(id);
    }
}
