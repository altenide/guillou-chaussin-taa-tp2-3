package sprint.web.rest;

import java.util.List;
import java.util.logging.Logger;

import sprint.domain.Team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sprint.service.interfaces.TeamService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/team")
public class TeamEndpoint {

    @Autowired
    private TeamService teamService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Team> getTeams() {
        return  teamService.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Team getTeam(@PathVariable("id") Long id) {
        return teamService.get(id);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Team addTeam(@RequestBody Team team) {
        return teamService.create(team);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Team updateTeam(@RequestBody Team team) {
        return teamService.update(team);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void removeUser(@PathVariable("id") Long id) {
        teamService.delete(id);
    }
}
