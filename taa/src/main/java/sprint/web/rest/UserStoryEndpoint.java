package sprint.web.rest;

import sprint.domain.UserStory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sprint.service.interfaces.UserStoryService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user_story")
public class UserStoryEndpoint {

    @Autowired
    private UserStoryService userStoryService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserStory> getUserStorys() {
        return userStoryService.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserStory getUserStory(@PathVariable("id") Long id) {
        return userStoryService.get(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserStory addUserStory(@RequestBody UserStory us)  {
        return userStoryService.create(us);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserStory updateUserStory(@RequestBody UserStory us)  {
        return userStoryService.update(us);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void removeUserStory(@PathVariable("id") Long id) {
        userStoryService.delete(id);
    }
}
