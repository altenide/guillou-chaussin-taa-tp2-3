package sprint.web.rest;

import java.util.List;

import sprint.domain.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import sprint.service.interfaces.TaskService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/task")
public class TaskEndpoint {

	@Autowired
	private TaskService taskService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> getTasks() {
		return  taskService.getAll();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Task getTask(@PathVariable("id") Long id) {
		return taskService.get(id);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Task addTask(@RequestBody Task task) {
		return taskService.create(task);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Task updateTask(@RequestBody Task task) {
		return taskService.update(task);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void removeUser(@PathVariable("id") Long id) {
		taskService.delete(id);
	}
}