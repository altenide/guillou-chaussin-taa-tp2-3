package sprint.web.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import sprint.domain.Developer;

import org.springframework.web.bind.annotation.*;

import sprint.service.interfaces.DeveloperService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/developer")
public class DeveloperEndpoint {

	@Autowired
	private DeveloperService developerService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Developer> getDevelopers() {
		return developerService.getAll();
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Developer getDeveloper(@PathVariable("id") Long id) {
        return developerService.get(id);
    }

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Developer addDeveloper(@RequestBody Developer developer) {
		return developerService.create(developer);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Developer updateDeveloper(@RequestBody Developer developer) {
		return developerService.update(developer);
	}

	@RequestMapping(value="{id}", method = RequestMethod.DELETE)
	public void removeUser(@PathVariable("id")Long id) {
		developerService.delete(id);
	}
}