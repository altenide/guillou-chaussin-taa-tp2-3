package sprint.logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LogAspect {

    @Around("execution(* sprint..*Endpoint.*(..))")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("Class : " + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("Arguments : " + Arrays.toString(joinPoint.getArgs()));

        Object res = joinPoint.proceed();
        System.out.println("Method " + joinPoint.getSignature().getName() + " with results " + res);

        return res;
    }
}
