Projet TAA 2015-2016
===================

Présentation
------------
Ce projet visait à développer l'architecture d'une application de gestion UserStory.

Lancement
------------

Pour lancer le serveur backend, placez vous dans le répertoire taa et exécutez la commande :

```
#!shell
mvn sprint-boot:run
```

Pour lancer le serveur frontend, placez vous dans le répertoire taa/wepapp du projet et exécutez la commande :

```
#!shell
grunt serve
```

Accès à l'API Rest
------------
Les entités accessiblent via une API Rest vont être: "user", "team", "user_story", "epic", "task"

Pour y accéder, plusieurs méthodes sont à votre disposition :


* GET:
```
#!shell

# Pour récupérer toutes les entités
url -H "Content-Type: application/json" -X GET http://localhost:8080/{{entityName}}

# Pour récupérer une donn&#233;e d'une entité
url -H "Content-Type: application/json" -X GET http://localhost:8080/{{entityName}}/{{entityId}}
```

* POST:  

```
#!shell

curl -H "Content-Type: application/json" -X POST -d '{parameters}' http://localhost:8080/{{entityName}}

```

* DELETE:

```
#!shell
curl -i -X DELETE http://localhost:8080/{{entityName}}/{{entityID}}

```

* UPDATE:

```
#!shell
curl -i -X PUT -H "Content-Type:application/json" http://localhost:8080/{{entityName}}/{{entityId}} -d '{newValues}'

```

Ce qui reste à faire
------------
La TP sur Docker n'a pas été ajouté au projet.